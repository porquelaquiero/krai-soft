/** @jsxImportSource @emotion/react */
import React, {useMemo} from 'react';
import PropTypes from 'prop-types';
import {css} from '@emotion/react';
import useWindowSize from '../hooks/useWindowSize';
import Draggable from "react-draggable";

const EightyPercentDiv = ({children, draggableComponentRef}) => {
    const {width: windowWidth, height: windowHeight} = useWindowSize();

    const [draggableComponentWidth, draggableComponentHeight] = useMemo(()=> {
        return [draggableComponentRef?.current?.clientWidth, draggableComponentRef?.current?.clientHeight];
    },[draggableComponentRef.current]);

    const totalViewportArea = windowWidth * windowHeight;
    const divHeight = windowHeight * 0.9;
    const divWidth = totalViewportArea * 0.8 / divHeight;

    const style = css`
        background: orange;
        width: ${divWidth}px;
        height: ${divHeight}px;
    `;

    return (
        <div css={style}>
            <Draggable
                bounds={{
                    top: 0,
                    left: 0,
                    right: divWidth - draggableComponentWidth,
                    bottom: divHeight - draggableComponentHeight
                }}
            >
                {children}
            </Draggable>
        </div>
    );
};

EightyPercentDiv.propTypes = {
    children: PropTypes.node,
    draggableComponentRef: PropTypes.oneOfType([
        PropTypes.func,
        PropTypes.shape({ current: PropTypes.any })
    ]),
};

export default EightyPercentDiv;
