import {useCallback, useEffect, useState} from "react";
import {isEqual} from "lodash";

const DEFAULT_STATE = {
    width: window.innerWidth,
    height: window.innerHeight,
};

export const getCurrentWindowSize = () => ({
    width: window.innerWidth,
    height: window.innerHeight,
});

const useWindowSize = () => {
    const [windowSize, setWindowSize] = useState(DEFAULT_STATE);

    const handleResize = useCallback(() => {
        setWindowSize(getCurrentWindowSize());
    }, []);

    useEffect(() => {

        window.addEventListener("resize", handleResize);

        handleResize();

        return () => window.removeEventListener("resize", handleResize);
    }, [handleResize]);

    useEffect(() => {
        const currentWindowSize = getCurrentWindowSize();

        if (!isEqual(windowSize, currentWindowSize)) {
            setWindowSize(currentWindowSize);
        }
    }, [windowSize]);

    return windowSize;
}

export default useWindowSize;