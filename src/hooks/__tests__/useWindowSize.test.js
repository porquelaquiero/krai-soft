import {useCallback, useEffect, useState} from "react";
import useWindowSize, {getCurrentWindowSize} from "../useWindowSize";

jest.mock("react", () => ({
    ...jest.requireActual('react'),
    useCallback: jest.fn(),
    useEffect: jest.fn(),
    useState: jest.fn(),
}));

describe("useWindowSize", () => {

    beforeEach(() => {
        jest.resetAllMocks();
    });

    describe("getCurrentWindowSize", () => {

        test("Should return window's width and height", () => {

            // Given
            window.innerWidth = 100;
            window.innerHeight = 100;

            // When
            const actual = getCurrentWindowSize();

            // Then
            expect(actual).toStrictEqual({
                width: 100,
                height: 100,
            })
        });

    });

    describe("useWindowSize", () => {

        const addEventListener = jest.fn();

        beforeEach(() => {
            jest.resetAllMocks();
            const spyWindowAddEventListener = jest.spyOn(window, 'addEventListener');
            spyWindowAddEventListener.mockImplementation(addEventListener);
        });

        test("Should add event listener and call the handleResize()", () => {
            // Given
            const handleResize = jest.fn();
            const windowSize = "";
            useState.mockReturnValue([windowSize, jest.fn()]);
            useCallback.mockReturnValue(handleResize);
            useEffect.mockImplementation(f => f());

            // When
            const actual = useWindowSize();

            // Then
            expect(actual).toBe(windowSize);
            expect(addEventListener).toBeCalledWith("resize", handleResize);
            expect(handleResize).toBeCalled();
        });

        test("Should re-set window size when windowSize is different with currentWindowSize", () => {

            // Given
            window.innerWidth = 100;
            window.innerHeight = 100;
            const handleResize = jest.fn();
            const setWindowSize = jest.fn();
            useState.mockReturnValue(["", setWindowSize]);
            useCallback.mockReturnValue(handleResize);
            useEffect.mockImplementation(f => f());

            // When
            useWindowSize();

            // Then
            expect(setWindowSize).toBeCalledWith({
                width: 100,
                height: 100,
            });
        });
    })


});
