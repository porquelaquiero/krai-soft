/** @jsxImportSource @emotion/react */
import React, {useRef} from 'react';
import {css} from '@emotion/react';
import EightyPercentDiv from '../components/EightyPercentDiv';

const MainPage = () => {
    const draggableComponentRef = useRef();

    const style = css`
        width: 100vw;
        height: 100vh;
        display: flex;
        justify-content: center;
        align-items: center;
    `;

    const draggableComponentStyle = css`
        width: 50px;
        height: 50px;
        display: flex;
        justify-content: center;
        align-items: center;
        background: yellow;
        cursor: pointer;
    `;

    return (
        <div css={style}>
            <EightyPercentDiv draggableComponentRef={draggableComponentRef}>
                <div css={draggableComponentStyle} ref={draggableComponentRef}>Drag</div>
            </EightyPercentDiv>
        </div>
    );
};

export default MainPage;
